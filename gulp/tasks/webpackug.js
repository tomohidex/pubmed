var gulp = require('gulp');
var uglify = require('gulp-uglify');
var webpack = require('webpack-stream');
var config = require('../config');

/*
 gulp webpack-ug
 */
gulp.task('webpackug', function () {
    config.webpack.output.filename = config.webpack.ugname;
    gulp.src(config.webpack.entry)
        .pipe(webpack(config.webpack))
        .pipe(uglify())
        .pipe(gulp.dest(config.js.dest));
});
