var gulp = require('gulp');
var webpack = require('webpack-stream');
var config = require('../config');

/*
gulp webpack
 */
gulp.task('webpack', function () {
    gulp.src(config.webpack.entry)
        .pipe(webpack(config.webpack))
        .pipe(gulp.dest(config.js.dest));
});
/*
 gulp webpack-watch
 */
gulp.task('webpack-watch', function () {
    gulp.watch(config.webpack.entry,['webpack']);
    gulp.watch(config.webpack.modules,['webpack']);
});