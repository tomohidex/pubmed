// @file config.js
var dest = './public/assets/js/build'; // 出力先ディレクトリ
var src = './public/assets';  // ソースディレクトリ
var webpack = require("webpack");
var path = require('path');


module.exports = {
    // 出力先の指定
    dest: dest,

    // jsのビルド設定
    js: {
        src: src + '/js/**',
        dest: dest + '',
        uglify: false
    },

    // webpackの設定
    webpack: {
        entry: src + '/js/app.js',
        output: {
            filename: 'bundle.js'
        },
        //追加項目 圧縮ビルド時のファイル名
        ugname:'bundle.min.js',
        //追加項目自作モジュールの場所
        modules:src + '/js/modules/**/**/*.{js,mustache}',

        //読み込むライブラリの場所、拡張子
        resolve: {
            root: ["bower_components"],
            extensions: ['', '.js']
        }//,
        //plugins: [
        //    new webpack.ResolverPlugin(
        //        new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin("bower.json", ["main"])
        //    ),
        //    //jqueryをグローバルに
        //    new webpack.optimize.DedupePlugin(),
        //    new webpack.optimize.AggressiveMergingPlugin(),
        //    new webpack.ProvidePlugin({//jqueryはグローバルに出す設定。これでrequireせず使えるのでjqueryプラグインもそのまま動く。
        //        jQuery: "jquery",
        //        $: "jquery",
        //        jquery: "jquery"
        //    })
        //]
    }
}